import React, { useContext } from "react"
import { Link } from "react-router-dom";
import { UserContext } from "../context/UserContext";
import {Button, Container} from '@material-ui/core';

const Header =() =>{
  const [user, setUser] = useContext(UserContext)
  const handleLogout = () =>{
    setUser(null)
    localStorage.removeItem("user")
  }


  return(    
    <header>
      <category>Categories</category>
      <nav>
        <ul>
          <li><Link className="active" to="/">Home</Link></li>
          { user && <li><Link to="/Movies">Movies Editor</Link></li>}
          { user && <li><Link to="/Game">Games Editor</Link></li> }
        </ul>
      </nav>
      <nav>
        <ul>
        { user === null &&<Button variant="contained" color="primary"> <la><Link className="log" to="/login">Login</Link></la> </Button>}
          { user === null && <li><Link to="/register">Register</Link></li> }
          { user && <li><Link to="/changepassword">Change Password</Link></li> }
          { user && <Button variant="contained" color="primary"><la><a style={{cursor: "pointer"}} onClick={handleLogout}>Logout </a></la> </Button>}
        </ul>
      </nav>
    </header>
  )
}

export default Header
