import React, {Component} from "react"
import axios from "axios"

class GameList extends Component {
  constructor(props){
    super(props)
    this.state = {
      movies: []
    }
  }

  componentDidMount(){
    axios.get(`https://backendexample.sanbersy.com/api/data-game`)
    .then(res => {
      let movies = res.data.map(el=>{ return {
        id: el.id, 
        name: el.name, 
        singlePlayer: el.singlePlayer,
        multiplayer: el.multiplayer,
        genre: el.genre,
        platform: el.platform,
        image_url: el.image_url
      }})
      this.setState({movies})
    })
  }

  render(){
    return (
      <>
        <h1>DAFTAR <i>GAME</i> <hh>TERBAIK</hh></h1>
        <div id="article-list">
          {
            this.state.movies.map((item)=>{
              return(
                <div>
                  <h3>{item.name}</h3>
                  <div style={{display: "inline-block"}}>
                    <div style={{width: "40vw", float: "left", display: "inline-block"}}>
                      <img style={{width: "100%", height: "300px", objectFit: "cover"}} src={item.image_url} />
                    </div>
                    <div style={{float: "left", "font-size": "20px", padding: "10px", top: 0, display: "inline-block" }}>
                      <strong>Single Player {item.singlePlayer}</strong><br/>
                      <strong>Multi Player: {item.multiplayer}</strong><br/>
                      <strong>genre: {item.genre}</strong>
                    </div>
                  </div>
                  <p>
                    <strong>Platform</strong>: 
                    {item.platform}
                  </p>
                  <hr/>
                </div>
              )
            })
          }
        </div>
      </>
    )
  }
}

export default GameList
